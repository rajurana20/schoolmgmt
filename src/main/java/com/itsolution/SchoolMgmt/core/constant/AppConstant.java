package com.itsolution.SchoolMgmt.core.constant;

public class AppConstant {
    public static final String DEFAULT_PASSWORD="12345";
    public static final String MSG="msg";
    public static final String STUDENT_SUCCESS_MSG="Student added successfully. Student Should visit his email to see password";
    public static final String TEACHER_SUCCESS_MSG="Student added successfully. Teacher shpuld visit his email to see password ";
    public static final String STUDENT_UPDATE_SUCCESS_MSG="Student updated successfully";
    public static final String TEACHER_UPDATE_SUCCESS_MSG="Student updated successfully";
    public static final int PAGE_SIZE=5;
    private AppConstant()
    {
    }
}
