package com.itsolution.SchoolMgmt.core.utils;

import lombok.experimental.UtilityClass;

import java.util.Random;

@UtilityClass
public class NumUtil {
    public int getFiveDigitRandomNumber()
    {
        Random random = new Random();
        return random.nextInt(100000);
    }
}
