package com.itsolution.SchoolMgmt.core.enums;

public enum Grade {
    I,
    II,
    III,
    IV,
    V,
    VI,
    VII,
    VIII,
    IX,
    X,
    XI,
    XII;

    public static Grade getGrade(String strGrade)
    {
        for (Grade grade: Grade.values())
        {
            if (grade.toString().equals(strGrade))
            {
                return grade;
            }
        }
        throw new RuntimeException("Grade Not Found");
    }
}
