package com.itsolution.SchoolMgmt.core.enums;

public enum UserType {
    ADMIN,
    TEACHER,
    STUDENT;
}
