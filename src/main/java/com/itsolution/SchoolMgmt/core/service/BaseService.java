package com.itsolution.SchoolMgmt.core.service;

import java.util.List;

public interface BaseService<T> {
    T save (T t);
    List<T> getAll();
    T update (T t);
    void delete(int id);
}
