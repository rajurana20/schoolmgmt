package com.itsolution.SchoolMgmt.user.bean;

import com.itsolution.SchoolMgmt.core.enums.UserType;
import com.itsolution.SchoolMgmt.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class UserDetailBean implements UserDetails {

    private final User user;

    public UserDetailBean(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (user.getUserType().equals(UserType.ADMIN))
        {
            return AuthorityUtils.createAuthorityList("ROLE_ADMIN");
        }
        if (user.getUserType().equals(UserType.STUDENT))
        {
           return AuthorityUtils.createAuthorityList("ROLE_STUDENT");
        }
        if (user.getUserType().equals(UserType.TEACHER))
        {
           return AuthorityUtils.createAuthorityList("ROLE_TEACHER");
        }
        return AuthorityUtils.NO_AUTHORITIES;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUserName();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public User getUser() {
        return user;
    }
}
