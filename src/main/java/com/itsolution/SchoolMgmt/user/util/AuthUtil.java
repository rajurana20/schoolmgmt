package com.itsolution.SchoolMgmt.user.util;

import com.itsolution.SchoolMgmt.user.bean.UserDetailBean;
import com.itsolution.SchoolMgmt.user.entity.User;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@UtilityClass
public class AuthUtil {
    public User getLoggerUser()
    {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object object = (UserDetailBean) authentication.getPrincipal();
        if (object instanceof UserDetailBean)
        {
            UserDetailBean userDetailBean = (UserDetailBean) object;
            return userDetailBean.getUser();
        }
        return null;
    }
}
