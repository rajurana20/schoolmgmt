package com.itsolution.SchoolMgmt.user.repository;

import com.itsolution.SchoolMgmt.core.enums.Grade;
import com.itsolution.SchoolMgmt.user.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student,Integer> {
    @Query("select s from Student s where s.name=:name")
    List<Student> getMyStudent(@Param("name") String name);

    List<Student> findByGrade(Grade grade);
}
