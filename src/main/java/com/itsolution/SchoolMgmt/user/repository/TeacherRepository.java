package com.itsolution.SchoolMgmt.user.repository;

import com.itsolution.SchoolMgmt.user.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher,Integer> {
}
