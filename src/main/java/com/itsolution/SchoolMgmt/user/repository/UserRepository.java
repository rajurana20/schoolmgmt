package com.itsolution.SchoolMgmt.user.repository;

import com.itsolution.SchoolMgmt.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    List<User> deleteByAssociateId(int id);
    User findByUserName(String email);
}
