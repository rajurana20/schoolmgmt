package com.itsolution.SchoolMgmt.user.service;

import com.itsolution.SchoolMgmt.core.service.BaseService;
import com.itsolution.SchoolMgmt.user.entity.Teacher;
import org.springframework.stereotype.Service;

@Service
public interface TeacherService extends BaseService<Teacher> {
    Teacher getById(int id);
}
