package com.itsolution.SchoolMgmt.user.service;

import com.itsolution.SchoolMgmt.core.enums.Grade;
import com.itsolution.SchoolMgmt.core.service.BaseService;
import com.itsolution.SchoolMgmt.user.entity.Student;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StudentService extends BaseService<Student> {
    Student getById(int id);
    List<Student> getByGrade(Grade grade);

}
