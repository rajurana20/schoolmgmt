package com.itsolution.SchoolMgmt.user.service.mail;

import com.itsolution.SchoolMgmt.core.constant.AppConstant;
import com.itsolution.SchoolMgmt.user.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class MailSendingService {
    @Autowired
    private JavaMailSender mailSender;

    public void sendEmailMessage(String username, String password) throws MailException
    {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom("ranasona2077@gmail.com");
        mailMessage.setTo(username);
        mailMessage.setSubject("Account Created Successfully");
        mailMessage.setText("Hello your username is : "+username+ "And Password is : " + password);
        new Thread(()->mailSender.send(mailMessage)).start();
    }
}
