package com.itsolution.SchoolMgmt.user.service;

import com.itsolution.SchoolMgmt.core.constant.AppConstant;
import com.itsolution.SchoolMgmt.core.enums.UserType;
import com.itsolution.SchoolMgmt.core.utils.NumUtil;
import com.itsolution.SchoolMgmt.user.entity.Teacher;
import com.itsolution.SchoolMgmt.user.entity.User;
import com.itsolution.SchoolMgmt.user.repository.TeacherRepository;
import com.itsolution.SchoolMgmt.user.repository.UserRepository;
import com.itsolution.SchoolMgmt.user.service.mail.MailSendingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class TeacherServiceImpl implements TeacherService{
    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MailSendingService mailSendingService;

    @Override
    public Teacher save(Teacher teacher) {
        teacherRepository.save(teacher);

        User user = new User();
        user.setUserName(teacher.getEmail());
        int randomNumber= NumUtil.getFiveDigitRandomNumber();
        user.setPassword(passwordEncoder.encode(""+randomNumber));
        user.setUserType(UserType.TEACHER);
        user.setAssociateId(teacher.getTeacherId());
        userRepository.save(user);
        mailSendingService.sendEmailMessage(user.getUserName(),""+randomNumber);
        return teacher;
    }

    @Override
    public List<Teacher> getAll() {
        return teacherRepository.findAll();

    }

    @Override
    public Teacher update(Teacher teacher) {
        return teacherRepository.save(teacher);
    }

    @Override
    @Transactional
    public void delete(int id) {
        teacherRepository.deleteById(id);
        userRepository.deleteByAssociateId(id);
    }

    @Override
    public Teacher getById(int id) {
        return teacherRepository.findById(id).orElse(new Teacher());
    }
}
