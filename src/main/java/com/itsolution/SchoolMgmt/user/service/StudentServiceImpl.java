package com.itsolution.SchoolMgmt.user.service;

import com.itsolution.SchoolMgmt.core.constant.AppConstant;
import com.itsolution.SchoolMgmt.core.enums.Grade;
import com.itsolution.SchoolMgmt.core.enums.UserType;
import com.itsolution.SchoolMgmt.core.utils.NumUtil;
import com.itsolution.SchoolMgmt.user.entity.Student;
import com.itsolution.SchoolMgmt.user.entity.User;
import com.itsolution.SchoolMgmt.user.repository.StudentRepository;
import com.itsolution.SchoolMgmt.user.repository.UserRepository;
import com.itsolution.SchoolMgmt.user.service.mail.MailSendingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Random;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    MailSendingService mailSendingService;

    @Override
    public Student save(Student student) {
        studentRepository.save(student);

        User user = new User();
        user.setUserName(student.getEmail());
        int randomNumber = NumUtil.getFiveDigitRandomNumber();
        user.setPassword(passwordEncoder.encode(""+randomNumber));
        user.setUserType(UserType.STUDENT);
        user.setAssociateId(student.getStudentId());
        userRepository.save(user);
        mailSendingService.sendEmailMessage(user.getUserName(),""+randomNumber);
        return student;
    }

    @Override
    public List<Student> getAll() {
        return studentRepository.findAll();
    }

    @Override
    public Student update(Student student) {
        return studentRepository.save(student);
    }

    @Override
    @Transactional
    public void delete(int id) {
        studentRepository.deleteById(id);
        userRepository.deleteByAssociateId(id);
    }
    @Override
    public Student getById(int id)
    {
        return studentRepository.findById(id).orElse(new Student());
    }

    @Override
    public List<Student> getByGrade(Grade grade) {
        return studentRepository.findByGrade(grade);
    }
}
