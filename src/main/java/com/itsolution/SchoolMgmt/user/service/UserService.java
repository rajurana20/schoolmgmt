package com.itsolution.SchoolMgmt.user.service;

import com.itsolution.SchoolMgmt.core.service.BaseService;
import com.itsolution.SchoolMgmt.user.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService extends BaseService<User> {

}
