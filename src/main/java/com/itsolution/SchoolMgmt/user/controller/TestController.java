package com.itsolution.SchoolMgmt.user.controller;

import com.itsolution.SchoolMgmt.user.entity.User;
import com.itsolution.SchoolMgmt.user.util.AuthUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {
    @RequestMapping("/")
    public String getPage(ModelMap map)
    {
        User user = AuthUtil.getLoggerUser();
        map.put("loggedUser",user);
        return "home";
    }
    @RequestMapping("/home")
    public String getHome()
    {
        return "home";
    }
    @RequestMapping("/accesserror")
    public String getAccessEror()
    {
        return "accessDenied";
    }
}
