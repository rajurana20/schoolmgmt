package com.itsolution.SchoolMgmt.user.controller;

import com.itsolution.SchoolMgmt.core.constant.AppConstant;
import com.itsolution.SchoolMgmt.user.entity.Student;
import com.itsolution.SchoolMgmt.user.entity.Teacher;
import com.itsolution.SchoolMgmt.user.service.StudentService;
import com.itsolution.SchoolMgmt.user.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/teacher")
public class TeacherController {
    @Autowired
    TeacherService teacherService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView getTeacherList()
    {
        ModelAndView mv = new ModelAndView("teacherlist");
        mv.addObject("teachers",teacherService.getAll());
        return mv;
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView getTeacherFormPage(@ModelAttribute(AppConstant.MSG) String msg)
    {
        ModelAndView mv = new ModelAndView("teacherform");
        if (!msg.isEmpty())
        {
            mv.addObject(AppConstant.MSG,msg);
        }
        return mv;
    }
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public String addTeacher(@ModelAttribute Teacher teacher, RedirectAttributes redirectAttributes)
    {
        teacherService.save(teacher);
        /*ModelAndView mv = new ModelAndView("redirect:/teacher/list");*/
        redirectAttributes.addFlashAttribute(AppConstant.MSG,AppConstant.TEACHER_SUCCESS_MSG);
        return "redirect:/teacher/list";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView updateTeacherForm(@ModelAttribute(AppConstant.MSG) String msg,@PathVariable int id)
    {
        ModelAndView mv = new ModelAndView("teacheredit");
        if (!msg.isEmpty())
        {
            mv.addObject(AppConstant.MSG,msg);
        }
        mv.addObject("teacher",teacherService.getById(id));
        return mv;
    }
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    public String updateTeacher(@ModelAttribute Teacher teacher, RedirectAttributes redirectAttributes)
    {
        teacherService.save(teacher);
       /* ModelAndView mv = new ModelAndView("redirect:/teacher/list");*/
        redirectAttributes.addFlashAttribute(AppConstant.MSG,AppConstant.TEACHER_UPDATE_SUCCESS_MSG);
        return "redirect:/teacher/edit/"+teacher.getTeacherId();
    }



    @RequestMapping(value = "/delete/{id}")
    public String deleteTeacher(@PathVariable int id)
    {
        teacherService.delete(id);
        return "redirect:/teacher/list";
    }
}
