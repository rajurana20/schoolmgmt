package com.itsolution.SchoolMgmt.user.controller;

import com.itsolution.SchoolMgmt.core.constant.AppConstant;
import com.itsolution.SchoolMgmt.user.entity.Student;
import com.itsolution.SchoolMgmt.user.entity.User;
import com.itsolution.SchoolMgmt.user.repository.StudentRepository;
import com.itsolution.SchoolMgmt.user.service.StudentService;
import com.itsolution.SchoolMgmt.user.util.AuthUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.itsolution.SchoolMgmt.user.service.mail.MailSendingService;

import javax.transaction.Transactional;
import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentController {

    @Autowired
    StudentService studentService;
    @Autowired
    StudentRepository studentRepository;

    @Transactional
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView getStudentList(@RequestParam(defaultValue = "0") int page)
    {
        //mv.addObject("students",studentService.getAll());
        Page<Student> data = studentRepository.findAll(PageRequest.of(page, AppConstant.PAGE_SIZE));
        ModelAndView mv = new ModelAndView("studentlist");
        List<Student> studentList = data.getContent();
        mv.addObject("students",data);
        return mv;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView getStudentFormPage(@ModelAttribute(AppConstant.MSG) String msg)
    {
        ModelAndView mv = new ModelAndView("studentform");
        if (!msg.isEmpty())
        {
            mv.addObject(AppConstant.MSG,msg);
        }
        return mv;
    }
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public String addStudent(@ModelAttribute Student student, RedirectAttributes redirectAttributes)
    {
        //ModelAndView mv = new ModelAndView("redirect:/student/add");
        //mv.addObject(AppConstant.MSG,AppConstant.STUDENT_SUCCESS_MSG);
        studentService.save(student);
        redirectAttributes.addFlashAttribute(AppConstant.MSG,AppConstant.STUDENT_SUCCESS_MSG);
        return "redirect:/student/add";
    }


    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView updateStudentForm(@ModelAttribute(AppConstant.MSG) String msg, @PathVariable int id)
    {
        ModelAndView mv = new ModelAndView("studentedit");
        mv.addObject("student",studentService.getById(id));
        if (!msg.isEmpty())
        {
            mv.addObject(AppConstant.MSG,msg);
        }
        return mv;
    }
    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    public String updateStudent(@ModelAttribute Student student, RedirectAttributes redirectAttributes)
    {
        //ModelAndView mv = new ModelAndView("redirect:/student/add");

        //mv.addObject(AppConstant.MSG,AppConstant.STUDENT_SUCCESS_MSG);
        studentService.save(student);
        redirectAttributes.addFlashAttribute(AppConstant.MSG,AppConstant.STUDENT_UPDATE_SUCCESS_MSG);
        return "redirect:/student/edit/"+student.getStudentId();
    }


    @RequestMapping("/delete/{id}")
    public String deleteStudent(@PathVariable int id)
    {
        studentService.delete(id);
        return "redirect:/student/list";
    }
    @RequestMapping("/get/{name}")
    @ResponseBody
    public List<Student> getByName(@PathVariable String name)
    {
        return studentRepository.getMyStudent(name);
    }
}
