package com.itsolution.SchoolMgmt.user.controller;

import com.itsolution.SchoolMgmt.user.entity.User;
import com.itsolution.SchoolMgmt.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserRepository userRepository;
    @RequestMapping("/delete/{id}")
    @Transactional
    public String deleteByAssociateId(@PathVariable int id)
    {
        List<User> users= userRepository.deleteByAssociateId(id);
        return "Successfully deleted";
    }
}
