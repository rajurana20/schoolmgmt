package com.itsolution.SchoolMgmt;

import com.itsolution.SchoolMgmt.core.constant.AppConstant;
import com.itsolution.SchoolMgmt.core.enums.UserType;
import com.itsolution.SchoolMgmt.user.entity.User;
import com.itsolution.SchoolMgmt.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class SchoolMgmtApplication {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(SchoolMgmtApplication.class, args);
	}

	private final String username = "admin@admin.com";

	@PostConstruct
	public void createSuperAdmin()
	{
		User user = userRepository.findByUserName(username);
		if (user==null)
		{
			user =new User();
			user.setUserName(username);
			user.setPassword(passwordEncoder.encode(AppConstant.DEFAULT_PASSWORD));
			user.setUserType(UserType.ADMIN);
			user.setAssociateId(0);
			userRepository.save(user);
		}
	}


}
