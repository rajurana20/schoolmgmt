package com.itsolution.SchoolMgmt.webservice.dto;

import com.itsolution.SchoolMgmt.user.entity.Student;
import lombok.Builder;
import lombok.Data;

public class ResponseDto {
    private Object data;
    private String message;
    private boolean success;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
