package com.itsolution.SchoolMgmt.webservice.restcontroller;

import com.itsolution.SchoolMgmt.user.entity.Student;
import com.itsolution.SchoolMgmt.user.entity.Teacher;
import com.itsolution.SchoolMgmt.user.service.TeacherService;
import com.itsolution.SchoolMgmt.webservice.dto.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/teacher")
public class TeacherRestController {
    @Autowired
    TeacherService teacherService;

    @GetMapping("/list")
    public ResponseEntity<?> getAll()
    {
        ResponseDto responseDto = new ResponseDto();
        List<Teacher> teacherList = new ArrayList<>();
        try {
            teacherList = teacherService.getAll();
            responseDto.setData(teacherList);
            responseDto.setSuccess(true);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            responseDto.setSuccess(false);
        }
        responseDto.setMessage("Fetching teacher....");

        return ResponseEntity.ok(responseDto);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<?> getTeacher(@PathVariable int id)
    {
        ResponseDto responseDto = new ResponseDto();
        Teacher teacher = new Teacher();
        try {
            teacher= teacherService.getById(id);
            responseDto.setData(teacher);
            responseDto.setSuccess(true);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            responseDto.setSuccess(false);
        }
        responseDto.setMessage("Fetching teacher by id ...");

        return ResponseEntity.ok(responseDto);
    }

    @PostMapping("/add")
    public ResponseEntity<?> addTeacher(@RequestBody Teacher teacher)
    {
        ResponseDto responseDto = new ResponseDto();
        try {
            teacherService.save(teacher);
            responseDto.setData(teacher);
            responseDto.setSuccess(true);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            responseDto.setSuccess(false);
        }
        responseDto.setMessage("Adding New Teacher ....");

        return ResponseEntity.ok(responseDto);
    }

    @PutMapping("/add")
    public ResponseEntity<?> updateTeacher(@RequestBody Teacher teacher)
    {
        ResponseDto responseDto = new ResponseDto();
        try {
            teacherService.update(teacher);
            responseDto.setData(teacher);
            responseDto.setSuccess(true);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            responseDto.setSuccess(false);
        }
        responseDto.setMessage("Updating Teacher ...");
        return ResponseEntity.ok(responseDto);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteTeacher(@PathVariable int id)
    {
        ResponseDto responseDto = new ResponseDto();
        try
        {
            teacherService.delete(id);
            responseDto.setSuccess(true);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            responseDto.setSuccess(false);
        }
        responseDto.setMessage("Deleting Teacher...");

        return ResponseEntity.ok(responseDto);
    }
}
