package com.itsolution.SchoolMgmt.webservice.restcontroller;

import com.itsolution.SchoolMgmt.core.enums.Grade;
import com.itsolution.SchoolMgmt.user.entity.Student;
import com.itsolution.SchoolMgmt.user.service.StudentService;
import com.itsolution.SchoolMgmt.webservice.dto.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import lombok.Builder;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/student")
public class StudentRestController {
    @Autowired
    StudentService studentService;

    @GetMapping("/list")
    public ResponseEntity<?> getAll()
    {
        ResponseDto responseDto = new ResponseDto();
        List<Student> studentList = new ArrayList<>();
        try
        {
             studentList= studentService.getAll();
             responseDto.setSuccess(true);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            responseDto.setSuccess(false);
        }

        responseDto.setMessage("Data Fetching ....");
        responseDto.setData(studentList);
        responseDto.setSuccess(true);
        return ResponseEntity.ok(responseDto);
    }
    @GetMapping("/get/{id}")
    public ResponseEntity<?> getById(@PathVariable int id)
    {
        ResponseDto responseDto = new ResponseDto();
        Student student = new Student();
        try {
            student = studentService.getById(id);
            responseDto.setData(student);
            responseDto.setSuccess(true);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            responseDto.setSuccess(false);
        }
        responseDto.setMessage("Fetching a student");
        return ResponseEntity.ok(responseDto);
    }

    @PostMapping("/add")
    public ResponseEntity<?> addStudent(@RequestBody Student student)
    {
        ResponseDto responseDto = new ResponseDto();
        try{
            studentService.save(student);
            responseDto.setSuccess(true);
            responseDto.setData(student);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            responseDto.setSuccess(false);
        }
        responseDto.setMessage("Creating Student...");
        return ResponseEntity.ok(responseDto);
    }

    @PutMapping("/add")
    public ResponseEntity<?> updateStudent(@RequestBody Student student)
    {
        ResponseDto responseDto = new ResponseDto();
        try {
            studentService.update(student);
            responseDto.setData(student);
            responseDto.setSuccess(true);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            responseDto.setSuccess(false);
        }

        responseDto.setMessage("Updating Student ....");
        return ResponseEntity.ok(responseDto);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteStudent(@PathVariable int id)
    {
        ResponseDto responseDto = new ResponseDto();
        try{
            studentService.delete(id);
            responseDto.setSuccess(true);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            responseDto.setSuccess(false);
        }
        responseDto.setMessage("Deleting student ....");
        return ResponseEntity.ok(responseDto);
    }

    @GetMapping("/getByGrade/{grade}")
    public ResponseEntity<?> getByGrade(@PathVariable String grade)
    {
        ResponseDto responseDto = new ResponseDto();
        List<Student> studentList = new ArrayList<>();
        try
        {
            studentList = studentService.getByGrade(Grade.getGrade(grade));
            responseDto.setData(studentList);
            responseDto.setSuccess(true);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            responseDto.setSuccess(false);
        }
       responseDto.setMessage("Fetching by grade ...");
        return ResponseEntity.ok(responseDto);
    }
}
